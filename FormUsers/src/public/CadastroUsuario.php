<?php

class CadastroUsuario{

    protected $dadosUsuario;
    protected $fileWithPath;
    public $loginOuEmailExistente;


    public function __construct(){
        $this->dadosUsuario = $_POST;
        $this->dadosUsuario["input-senha"] = password_hash($this->dadosUsuario["input-senha"], PASSWORD_DEFAULT);
        $this->fileWithPath = getcwd() . "/../../data/registros.txt";
    }

    public function getDadosCadastro(){
        return file_get_contents($this->fileWithPath);
    }

    public function setDadosCadastro(){
        if(!file_exists($this->fileWithPath)){
            return $this->criaArquivoGravaDados();
        } else{
            if($this->verificaExistenciaLoginEmail()){
                return $this->atualizaDadosDoArquivo();
            } else {
                $this->loginOuEmailExistente = true;
                return false;
            }
        }
    }

    protected function criaArquivoGravaDados(){
        $myfile = fopen($this->fileWithPath, "w");
        $arrayParaGravarDadosIniciais = array();
        $arrayParaGravarDadosIniciais["dadosUsuario"][] = array($this->dadosUsuario["input-login"] => $this->dadosUsuario);
        $jsonDadosUsuario = json_encode($arrayParaGravarDadosIniciais);
        return fwrite($myfile, $jsonDadosUsuario);
    }

    protected function verificaExistenciaLoginEmail(){
        $conteudoAtualArquivo = file_get_contents($this->fileWithPath);
        return (strpos($conteudoAtualArquivo,$this->dadosUsuario["input-login"]) === false) && (strpos($conteudoAtualArquivo,$this->dadosUsuario["input-email"]) === false);
    }

    protected function atualizaDadosDoArquivo(){
        $conteudoAtualArquivo = file_get_contents($this->fileWithPath);
        $arrayConteudoAtualArquivo = json_decode($conteudoAtualArquivo, true);
        $arrayConteudoAtualArquivo["dadosUsuario"][] = array($this->dadosUsuario["input-login"] => $this->dadosUsuario);
        file_put_contents($this->fileWithPath, "");
        $myfile = fopen($this->fileWithPath, "w");
        return fwrite($myfile, json_encode($arrayConteudoAtualArquivo));
    }
}

$cadastro = new CadastroUsuario();
if($cadastro->setDadosCadastro()){
    print "Registro inserido com sucesso!";
} else{
    if($cadastro->loginOuEmailExistente){
        print "Email ou login já existente. Não foi possível inserir registro";
    } else {
        print "Falha ao inserir registro! Contate o administrador.";
    }
}

?>