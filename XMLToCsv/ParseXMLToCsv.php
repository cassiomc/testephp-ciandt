<?php

class ParseXMLToCsv{

    protected $XMLtext;

    public function __construct(){
        $this->XMLtext = "<note>
        <to>CIAndT</to>
        <from>Cassio</from>
        <heading>Parser</heading>
        <body>Arquivo XML para ser transformado em CSV</body>
        </note>";
        $this->XMLFileWithPath = getcwd() . "/XMLToCsv/XMLFile.xml";
        $this->CSVFileWithPath = getcwd() . "/XMLToCsv/CSVFile.csv";
    }

    public function createXMLFile(){
        if(!file_exists($this->XMLFileWithPath)){
            $myfile = fopen($this->XMLFileWithPath, "w");
            fwrite($myfile, $this->XMLtext);
        }            
    }

    public function XMLParser(){
        $colunasCsv = "";
        $valoresCsv = "";
        $simpleXMLObject = simplexml_load_file($this->XMLFileWithPath);
        foreach($simpleXMLObject as $chave => $valor){
            $colunasCsv = $colunasCsv . $chave . ", ";
            $valoresCsv = $valoresCsv . $valor . ", ";
        }
        $colunasCsv = substr($colunasCsv, 0, -2) . "\n";
        $valoresCsv = substr($valoresCsv, 0, -2);
        return $colunasCsv . $valoresCsv;
    }

    public function createCSVFile($CSVText){
        if(!file_exists($this->CSVFileWithPath)){
            $myfile = fopen($this->CSVFileWithPath, "w");
            fwrite($myfile, $CSVText);
        }            
    }

}

$parser = new ParseXMLToCsv();
$parser->createXMLFile();
$parser->createCSVFile($parser->XMLParser());

?>