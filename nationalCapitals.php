<?php

class NationalCapitals{

    protected $location;

    public function __construct(){
        $this->location = array();
    }

    public function getCapitalsArray(){
        $capitals = array();
        $capitals = array_column($this->location, "capital");
        array_multisort($capitals, SORT_ASC, $this->location);
        return $this->location;
    }

    public function pushCapitalsArray($countryAndCapital){
        array_push($this->location,$countryAndCapital);
    }
}

$capitals = new NationalCapitals();
$capitals->pushCapitalsArray(["country"=>"Brazil","capital" => "Brasília"]);
$capitals->pushCapitalsArray(["country"=>"Argentina","capital" => "Buenos Aires"]);
$capitals->pushCapitalsArray(["country"=>"United States","capital" => "Washington"]);
$capitals->pushCapitalsArray(["country"=>"Ecuador","capital" => "Quito"]);
$capitals->pushCapitalsArray(["country"=>"Australia","capital" => "Canberra"]);

foreach($capitals->getCapitalsArray() as $countryCapital){
    print "A capital do país " . $countryCapital["country"] . " é " . $countryCapital["capital"] . ".\n";
}

?>