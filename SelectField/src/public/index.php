<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Registro de Usuários</title>
  </head>
  <body>
    <div class="container-fluid">
      <h1>Formulário para demonstrar criação de Select Field</h1>
      <form id="formRegistroUsuario" action="#" class="row g-3">
        <div class="col-md-6">
        <label for="inputNome" class="form-label">Nome</label>
        <input type="text" name="input-nome" class="form-control" id="inputNome">
        </div>
        <div class="col-md-6">
        <label for="inputSobrenome" class="form-label">Sobrenome</label>
        <input type="text" name="input-sobrenome" class="form-control" id="inputSobrenome">
        </div>
        <div class="col-md-6">
        <label for="inputEmail" class="form-label">Email</label>
        <input type="email" name="input-email" class="form-control" id="inputEmail">
        </div>
        <div class="col-md-6">
        <label for="inputTelefone" class="form-label">Telefone - <em>format: 99-9999-9999 ou 99-99999-9999</em></label>
        <input type="text" name="input-telefone" class="form-control" id="inputTelefone">
        </div>
        <div class="col-md-6">
        <label for="inputLogin" class="form-label">Login</label>
        <input type="text" name="input-login" class="form-control" id="inputLogin">
        </div>                
        <div class="col-md-6">
        <label for="inputSenha" class="form-label">Senha</label>
        <input type="password" name="input-senha" class="form-control" id="inputSenha">
        </div>
        <div class="row row-cols-2 p-5">
            <div class="col-md-6">
                <button type="button" onclick="criarSelectField()" class="btn btn-success col-md-6">Criar Select Field</button>
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary col-md-6">Cadastrar</button>
            </div>
        </div>
      </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
        function criarSelectField(){
            var jqxhr = $.ajax(window.location.pathname +"SelectFieldCreator.php")
            .done(function(select) {
                $("#formRegistroUsuario").prepend(select);
            })
            .fail(function() {
                alert( "error" );
            });
        }
    </script>
  </body>
</html>