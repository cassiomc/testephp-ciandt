<?php

class SelectFieldCreator{

    protected $selectField;

    public function __construct(){
        $this->divWithSelectFieldAndLabel = "<div class='col-md-6'>
        <label class='form-label'>Select Dinâmico</label>
        <select class='form-select' aria-label='Default select example'>
            <option selected>Open this select menu</option>
            <option value='1'>One</option>
            <option value='2'>Two</option>
            <option value='3'>Three</option>
        </select></div>";
    }

    public function retornaSelectField(){
        return $this->divWithSelectFieldAndLabel;
    }
}

$selectFieldCreatorObject = new SelectFieldCreator();
echo $selectFieldCreatorObject->retornaSelectField();
?>