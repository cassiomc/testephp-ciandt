<?php

class JohnBites{

    protected $bites;
    protected $arrayBites;

    public function __construct(){
        $this->bites = rand();
        if($this->bites % 2 !== 0){
            $this->bites = $this->bites + 1;
        }
    }

    public function foiMordido(){
        return $this->arrayBites = array("total"=> $this->bites, "bited"=>floor($this->bites/2), "notBited"=>floor($this->bites/2));
    }
}

$bitesOfJohn = new JohnBites();
print "Em " . $bitesOfJohn->foiMordido()["total"] . " ocorrências \n";
print "Joãozinho mordeu o seu dedo !" . " -----> " . $bitesOfJohn->foiMordido()["bited"] . " vezes.\n";
print "Joaozinho NAO mordeu o seu dedo !" . " -----> " . $bitesOfJohn->foiMordido()["notBited"] . " vezes.\n";

?>