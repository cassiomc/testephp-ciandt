<?php

class FilesExtension{

    protected $filesNamesWithExtension;
    protected $arrayExtensions;

    public function __construct(){
        $this->filesNamesWithExtension = array("music.mp4","video.mov","imagem.jpeg","teste.aif");
        $this->arrayExtensions = array();
    }

    public function createFiles(){
        foreach($this->filesNamesWithExtension as $file){
            if(!file_exists(getcwd() . "/FilesExt/" . $file)){
                fopen(getcwd() . "/FilesExt/" . $file, "w");
            }            
        }
    }

    public function getFilesExtension(){
        foreach($this->filesNamesWithExtension as $file){
            array_push($this->arrayExtensions, pathinfo($file, PATHINFO_EXTENSION));
        }
        sort($this->arrayExtensions);
        return $this->arrayExtensions;
    }

}

$files = new FilesExtension();
$files->createFiles();
foreach($files->getFilesExtension() as $index => $extension){
    print($index+1 . " ." . $extension . "\n");
}

?>